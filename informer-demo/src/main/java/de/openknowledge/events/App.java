package de.openknowledge.events;

import com.squareup.okhttp.OkHttpClient;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.informer.ResourceEventHandler;
import io.kubernetes.client.informer.SharedIndexInformer;
import io.kubernetes.client.informer.SharedInformerFactory;
import io.kubernetes.client.models.V1Event;
import io.kubernetes.client.models.V1EventList;
import io.kubernetes.client.models.V1ObjectMeta;
import io.kubernetes.client.util.CallGeneratorParams;
import io.kubernetes.client.util.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.Validate.notEmpty;

public class App {
  private static final Logger LOG = LoggerFactory.getLogger(App.class);

  private static CoreV1Api coreV1Api;

  private static V1Event newEvent;

  public static void main(String[] args) {
    notEmpty(args, "No config provided. Give the path to a kubernetes config");
    // setup the connection to the k8 cluster
    setupAPI(args[0]);

    // create an EventInformer
    SharedInformerFactory factory = new SharedInformerFactory();
    SharedIndexInformer<V1Event> eventInformer = createEventInformer(factory);

    eventInformer.addEventHandler(new ResourceEventHandler<V1Event>() {
      @Override
      public void onAdd(V1Event event) {
        LOG.info("{}", event);
        if (event
            .getMetadata()
            .getName()
            .contains("new Event")) {
          newEvent = event;
        }
      }

      // Events are always added. No update or delete needed
      @Override
      public void onUpdate(V1Event oldEvent, V1Event newEvent) {
      }

      @Override
      public void onDelete(V1Event event, boolean deletedFinalStateUnknown) {
      }
    });

    factory.startAllRegisteredInformers();

    // fire an event
    fireEvent();
  }

  /**
   * Creates and starts an Event
   */
  private static void fireEvent() {
    try {
      V1Event eventToCreate = new V1Event();
      V1ObjectMeta metadata = new V1ObjectMeta();
      // set unique name
      metadata.setName("new Event " + LocalDateTime.now());
      eventToCreate.setMetadata(metadata);

      coreV1Api.createNamespacedEvent("default", eventToCreate, null, null, null);

    } catch (ApiException a) {
      LOG.warn(a.getMessage(), a);
    }

  }

  /**
   * Creates an EventInformer
   *
   * @param factory
   * @return
   */
  private static SharedIndexInformer<V1Event> createEventInformer(final SharedInformerFactory factory) {
    return factory.sharedIndexInformerFor((CallGeneratorParams params) -> {
      try {
        return coreV1Api.listEventForAllNamespacesCall(null, null, null, null, null, null, params.resourceVersion, params.timeoutSeconds, params.watch, null, null);
      } catch (ApiException e) {
        LOG.error(e.getMessage(), e);
        return null;
      }
    }, V1Event.class, V1EventList.class);
  }

  /**
   * Setups the Kubernetes API with the given config
   *
   * @param arg config
   * @throws IOException
   */
  private static void setupAPI(final String arg) {
    try {
      ApiClient apiClient = Config.fromConfig(arg);

      Configuration.setDefaultApiClient(apiClient);

      OkHttpClient httpClient = apiClient.getHttpClient();
      httpClient.setReadTimeout(10, TimeUnit.MINUTES);
      apiClient.setHttpClient(httpClient);

      coreV1Api = new CoreV1Api();
    } catch (IOException i) {
      LOG.error(i.getMessage(), i);
    }
  }

  public static V1Event getNewEvent() {
    return newEvent;
  }
}
