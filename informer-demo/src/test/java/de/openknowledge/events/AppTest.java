package de.openknowledge.events;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AppTest {

  @Test
  public void testMain() throws InterruptedException {
    String pathToConfig = System.getProperty("config");
    App.main(new String[] { pathToConfig });

    Thread.sleep(2000);

    assertThat(App.getNewEvent()).isNotNull();

    assertThat(App
        .getNewEvent()
        .getMetadata()
        .getName()
        .contains("new Event")).isTrue();
  }
}
