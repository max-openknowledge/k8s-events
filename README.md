# Kubernetes Events

This repository is a collection of ways to collect events in Kubernetes.


## Informer Demo(fabric8)

In this demo application the fabirc8 kubernetes client is being used to create an `EventInformer`,
which reacts to the creation of events inside all namespaces.

### Run the app

Run:
```bash
mvn clean package
```

```bash
java -jar target/informer-fabric8-demo-1.0-SNAPSHOT.jar 
```

You can hit `Control + C` to stop listening

## Informer Demo

In this demo application the official kubernetes client is being used to create an `EventInformer`,
which reacts to the creation, updating and deleting of events inside all namespaces.

### Run the app

First you need a valid kubernetes config. It should be here: `/Users/USER/.kube/config`.

Run:
```bash
java -jar informer-demo-1.0-SNAPSHOT-jar-with-dependencies.jar /Users/USER/.kube/config
```

You can hit `Control + C` to stop listening

## Kubernetes Event Exporter (Opsgenie)

Here is a [Link to the exporter](https://github.com/opsgenie/kubernetes-event-exporter).
