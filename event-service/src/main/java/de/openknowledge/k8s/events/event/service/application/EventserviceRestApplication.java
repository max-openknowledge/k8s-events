package de.openknowledge.k8s.events.event.service.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class EventserviceRestApplication extends Application {
}
