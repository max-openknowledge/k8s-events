package de.openknowledge.k8s.events.event.service.domain.event;

import io.fabric8.kubernetes.api.model.Event;
import io.fabric8.kubernetes.api.model.EventList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.informers.ResourceEventHandler;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.fabric8.kubernetes.client.informers.SharedInformerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

@Startup
@Singleton
@ApplicationScoped
public class K8sInformer {
  private static final Logger LOG = LoggerFactory.getLogger(K8sInformer.class);

  @PostConstruct
  public void startUp() {
    try (final KubernetesClient client = new DefaultKubernetesClient()) {
      SharedInformerFactory sharedInformerFactory = client.informers();
      SharedIndexInformer<Event> eventInformer = sharedInformerFactory.sharedIndexInformerFor(Event.class, EventList.class, 30 * 1000L);

      eventInformer.addEventHandler(new ResourceEventHandler<Event>() {
        @Override
        public void onAdd(final Event event) {
          LOG.info(event.toString());
        }

        @Override
        public void onUpdate(final Event event, final Event t1) {

        }

        @Override
        public void onDelete(final Event event, final boolean b) {

        }
      });

      LOG.info("Informer added");

      sharedInformerFactory.startAllRegisteredInformers();
    }
  }
}
