package de.openknowledge.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.fabric8.kubernetes.api.model.Event;
import io.fabric8.kubernetes.api.model.EventList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.informers.ResourceEventHandler;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.fabric8.kubernetes.client.informers.SharedInformerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class App {
  private static final Logger LOG = LoggerFactory.getLogger(App.class);

  public static void main(String[] args) throws InterruptedException {
    try (final KubernetesClient client = new DefaultKubernetesClient()) {
      SharedInformerFactory sharedInformerFactory = client.informers();
      SharedIndexInformer<Event> eventInformer = sharedInformerFactory.sharedIndexInformerFor(Event.class, EventList.class, 30 * 1000L);

      eventInformer.addEventHandler(new ResourceEventHandler<Event>() {
        @Override
        public void onAdd(final Event event) {
          try {

            LOG.info("{}", new ObjectMapper().writeValueAsString(event));
          } catch (Exception e) {
            LOG.warn(e.getMessage(), e);
          }
        }

        @Override
        public void onUpdate(final Event event, final Event t1) {

        }

        @Override
        public void onDelete(final Event event, final boolean b) {

        }
      });

      LOG.info("Informer added");

      sharedInformerFactory.startAllRegisteredInformers();

      TimeUnit.MINUTES.sleep(3);
    }
  }
}
